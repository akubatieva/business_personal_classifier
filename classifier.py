import os
import sys

from sklearn.externals import joblib

from data_preprocessing import preprocess


def read_input():
    if len(sys.argv) != 2:
        print("You should pass input filename as a parameter to this script")
        exit()
    else:
        filename = sys.argv[1]
        if os.path.exists(filename):
            with open(filename, 'r') as f:
                try:
                    return f.read()
                except IOError:
                    print("Can't read file! Exiting")
                    exit()
        else:
            print("File does not exist. Exiting")
            exit()


CLASS_PERSONAL = 0
CLASS_BUSINESS = 1
MARGIN = 0.4


def predict(doc):
    model = joblib.load('classification_model.pkl')
    preprocessed_text = [preprocess(doc)]
    proba = model.predict_proba(preprocessed_text).tolist()[0]
    business_probability = proba[CLASS_BUSINESS]
    if business_probability >= MARGIN:
        result = "business"
    else:
        result = "personal"
    return result


if __name__ == "__main__":
    text = read_input()
    label = predict(text)
    print(label)
