##Business / Personal Communication Classifier

###Prerequisites 

1. (Optional) Create virtual environment and activate it (I used Python 3.5 interpreter)
2. Install Python developer package. It's Python version dependent, so in my case it was:
`sudo apt-get install python3.5-dev`
3. Install dependencies from requirements.txt using pip or PyCharm built-in tools
4. Install SpaCy model for English language:
`python -m spacy download en`

###Using

To use the classifier run following command in terminal:  
`python classifier.py path/to/your/file`  
The output will be **business** or **personal** respectively.  
(first run could be slower than next ones because of updating stopwords package)
###Project Structure


**data** directory contains dataset used for training and testing.  
**classifier.ipynb** contains model source code, some visualization and exports model to **classification_model.pkl**.  
**classifier.py** - use model for providing output.  
**data_preprocessing.py** contains functions for data preprocessing.  
**input.txt** - sample input file.




##Classifier Description

###Data Set

Train dataset contains of 2 classes: "business" and "personal". However, initial data seems to be unbalanced: 4871 business texts vs. 1855 personal texts. So, I've added 3000 texts from [Twitter dataset](https://data.world/data-society/twitter-user-data) (data/gender-classifier-DFE-791531.csv) as "personal" texts to balance the classes.

###Data Preprocessing

Since initial dataset consists of emails, it was necessary to get the body of every email because it’ll be more relevant for text classification. Hence, “senders”, “receivers”, “copies” information etc. was cleaned. For Twitter data hashtags were also removed from the text.

After cleaning, dataset was tokenized, tokens containing non-alphabetical characters and stop-words were removed, the rest tokens were lemmatized and stemmed.

After exploring and normalizing texts were transformed to **TF-IDF** vectors which can be used as input for classification algorithms. The TF-IDF measure was selected because some unigrams and bigrams seems to be more relevant for "business" texts than for "personal" ones and vice versa.

###Classifiers & Model Selection

LinearSVC, LogisticRegression, MultinomialNB and RandomForestClassifier usually display good results for text classification so they were chosen for evaluation.

I used **5-fold cross-validation with F-measure** as evaluation metrics for model selection. It showed following results:

LinearSVC 0.962000  
LogisticRegression 0.957908   
MultinomialNB 0.928471  
RandomForestClassifier 0.809921

Since **LinearSVC** showed best results, it was selected for classification.

###Classification Issue

The main classification issue are "mixed" texts, which contain both business and personal content. In such cases it'll be better to mark texts containing business-related discussions as "business". To solve this issue I've tried to implement multi-label classification (one-vs.-rest strategy in particular) but it didn't predict both “business” and “personal” labels for any text in test dataset. In fact there wasn’t any sense to use one-vs.-rest strategy in this case, because it wasn’t proper multi-labeling.

I assume that the reason is train dataset, as it should contain texts labeled as business and personal simultaneously.

To solve this problem I decided to use **CalibratedClassifierCV** as a wrapper for LinearSVC to see the predicted probabilities of class membership. Then I set a margin equal to 0.4 which allows to mark text as “business” if probability of “business” label for this text is equal or more then 40%.
This test is separated from model because in this case it’ll be easier to change margin and not affect the model itself.

The margin-test seems to be a rough way to solve the problem and, perhaps, some data expanding with more accurate labeling + multi-labeling strategy might be better for further development.

	 	 	
###Results

The pipeline (TF-IDF + CalibratedClassifierCV + LinearSVC) showed quite high accuracy score on train and test set:

Train set accuracy – 1.00  
Test set accuracy - 0.96
