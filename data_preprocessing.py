import re

import nltk
import spacy
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

nltk.download('stopwords', quiet=True)


def clean_noise(text_list):
    noisy_str = ("from:", "to:", "cc:", "bcc:", "date:", "x-filename:", "<markup",
                 "mime-version:", "content-type:", "content-transfer-encoding:", "x-from:",
                 "x-to:", "x-cc:", "x-bcc:", "x-folder:", "x-origin:", "x-filename:", "message-id:")
    subject_str = "subject:"
    final_text = []
    for text in text_list:
        lower = text.lower()
        if lower.startswith(subject_str):
            subject_substr = text[len(subject_str):]
            final_text.append(subject_substr)
        elif not lower.startswith(noisy_str):
            final_text.append(text)
    final_str = ' '.join(final_text)
    return final_str


stopwords = set(stopwords.words('english'))
stemmer = SnowballStemmer("english")
en_nlp = spacy.load('en')


def preprocess(text):
    text = remove_hashtags(text)
    result = []
    for token in en_nlp(text, disable=['parser', 'tagger', 'ner']):
        lemma = token.lemma_
        if lemma.lower() not in stopwords and len(lemma) > 3 and token.is_alpha:
            result.append(get_stem(lemma))
    return ' '.join(result)


def remove_hashtags(text):
    text = re.sub(
        r'\B(#[a-zA-Z]+\b)',
        '',
        text)
    return text


def get_stem(lemma):
    return stemmer.stem(lemma)
